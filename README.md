![icon.png](https://bitbucket.org/repo/4LgndM/images/2588019440-icon.png)

# Food Friends #

Platform: Android and iOS

Projects: foodFriend, foodFriend.Droid, foodFriend.iOS

Food Friends is your personal assistant application when it comes to food. This application helps you with showing the restaurants available around Melbourne, restaurant that you preferred, top 10 restaurants of the week, directions to that restaurant you want, and for those of you who wants to cook, you got special recipes for you to make by yourself. Our main goal was to satisfy your needs when it comes to food, whether by showing restaurants or recipes.

License File: [Licenses](https://bitbucket.org/z57909160/foodfriend/src/19ebe046ac8e88f4b7e406c8d3e16114be9efa40/Licenses.txt?at=master&fileviewer=file-view-default)

Trello Link: [Food Friends Trello](https://trello.com/b/wETFrZig/food-friend-sit313)

Demo Video Link: [Youtube](https://youtu.be/78YGAhq61PA)


### To download or clone, you can use SourceTree and open the file with Xamarin Studio. When app is run on simulator or phone, maps and internet are expected to make the best of use of the features. ###
NOTE: If there are errors with packages when project first run, simply Clean the project and then Rebuild it before Deploy, then all packages required will be restored automatically.

Technologies Used: Xamarin Forms, Xamarin Maps, SQLite, Master Details Page, Microsoft Azure, Tabbed View, Plugins Popup, and List View.

Members: Aldrich Clarence (aclarenc), Clark Zhu (yrq), Tai Phu Duong (ptduong)


### ******************* Data Synchronization feature explanation*************************###
1. check internet connectivity
2. online(internet available) - async from Azure service
   2.1 check the database version of online and local database
   2.1.1 if matches, do nothing
   2.1.2 if different, update the local database to the online one
3. offline(no internet service) - using local database