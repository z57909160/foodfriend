﻿/*
 * Project Name: food Friends
 */
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace foodFriend
{

    public partial class TopTenRes : ContentPage
    {
        //initialise all variables
        ORestaurantManager online_manager;
        IEnumerable<ORestaurant> onlineRes = null;
        IEnumerable<ORestaurant> toptenonline = null;
        RestaurantDB local_manager = new RestaurantDB();
        IEnumerable<Restaurant> localTopTen = null;

        
        public TopTenRes()
        {
            InitializeComponent();

			Title = "Weekly Top 10 Restaurants";

            online_manager = ORestaurantManager.DefaultManager;
            local_manager = new RestaurantDB();
        }
        //what to do when onappearing the app
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            /*
            var checkInternet = DependencyService.Get<CheckInternet>();
            checkInternet.CheckNetworkConnection();
            */
            isConnected();
        }

        // Data methods
        async Task AddItem(ORestaurant item)
        {
            await online_manager.SaveTaskAsync(item);
            _restaurantList.ItemsSource = await online_manager.GetRestaurantAsync();
        }







        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#pulltorefresh
        //fresh items in listview
        public async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            Exception error = null;
            try
            {
                await RefreshItems(false, true);
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                list.EndRefresh();
            }

            if (error != null)
            {
                await DisplayAlert("Refresh Error", "Couldn't refresh data (" + error.Message + ")", "OK");
            }
        }
        //what to do when async data
        public async void OnSyncItems(object sender, EventArgs e)
        {
            await RefreshItems(true, true);
        }
        //initialise listview
        public async void initialise(object sender, EventArgs e)
        {
            await online_manager.InitialiseDB();
            await RefreshItems(true, true);
        }

        //check the version of local and online database
        public async void checkversion()
        {
            int localVersion;
            int onlineVersion;
            if (onlineRes != null && onlineRes.Count() > 0)
            {
                localVersion = local_manager.GetCurrentVersion();
                onlineVersion = onlineRes.First().DBVersion;

                if (onlineRes.First().DBVersion == local_manager.GetCurrentVersion())
                {
                }
                else
                {
                    await local_manager.UpdateDB(onlineRes, onlineVersion);
                    await DisplayAlert("Version Changes", "Version is updated from Version "
                        + localVersion.ToString() + " Version " + onlineVersion.ToString(), "OK");
                }
            }
        }
        //refreshing listview items
        private async Task RefreshItems(bool showActivityIndicator, bool syncItems)
        {

            using (var scope = new ActivityIndicatorScope(syncIndicator, showActivityIndicator))
            {
                // All of the restaurants online
                onlineRes = await online_manager.GetRestaurantAsync(syncItems);
                // Top ten of online restaurants
                toptenonline = onlineRes.Where(t => t.Ranking <= 10);
                _restaurantList.ItemsSource = toptenonline;
                _restaurantList.ItemTemplate = new DataTemplate(typeof(TextCell));
                _restaurantList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
                _restaurantList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
                _restaurantList.BackgroundColor = Color.FromHex("#fcecdf");


                checkversion();

            }



        }

        //check if internet is available
        public async void isConnected()
        {
            /*
            if (response.IsSuccessStatusCode) 
                await DisplayAlert("IsConnected", "Yes", "OK");
            else { await DisplayAlert("IsConnected", "No", "OK"); }
            */
            using (var scope = new ActivityIndicatorScope(syncIndicator, true))
            {
                bool isInternetOK = await CrossConnectivity.Current.IsRemoteReachable("https://testappforms.azurewebsites.net", 80, 3000);
                if (!isInternetOK)
                {
                    var response = await DisplayAlert("Connection Error", "Internet Not Available, Do you want to use local database?", "Yes", "Cancel");
                    if (response)
                    {
                        localTopTen = local_manager.GetTopTen();
                        _restaurantList.ItemsSource = localTopTen;
                        _restaurantList.ItemTemplate = new DataTemplate(typeof(TextCell));
                        _restaurantList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
                        _restaurantList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Category");
                        _restaurantList.BackgroundColor = Color.FromHex("#fcecdf");
                    }
                }
                else
                {
                    // Set syncItems to true in order to synchronize the data on startup when running in offline mode
                    await RefreshItems(true, syncItems: false);
                }

            }



        }

        //in processing indicator
        private class ActivityIndicatorScope : IDisposable
        {
            private bool showIndicator;
            private ActivityIndicator indicator;
            private Task indicatorDelay;

            public ActivityIndicatorScope(ActivityIndicator indicator, bool showIndicator)
            {
                this.indicator = indicator;
                this.showIndicator = showIndicator;

                if (showIndicator)
                {
                    indicatorDelay = Task.Delay(2000);
                    SetIndicatorActivity(true);
                }
                else
                {
                    indicatorDelay = Task.FromResult(0);
                }
            }

            private void SetIndicatorActivity(bool isActive)
            {
                this.indicator.IsVisible = isActive;
                this.indicator.IsRunning = isActive;
            }

            public void Dispose()
            {
                if (showIndicator)
                {
                    indicatorDelay.ContinueWith(t => SetIndicatorActivity(false), TaskScheduler.FromCurrentSynchronizationContext());
                }
            }




        }
    }
}