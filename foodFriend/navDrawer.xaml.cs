﻿/*
 * Project Name: food Friends
 */

using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace foodFriend
{
	public partial class navDrawer : MasterDetailPage	
	{
		public navDrawer()
		{
			// Use navDrawer.xaml that contains buttons, text, and boxview.
			InitializeComponent();

			// A method set when allRestaurantsBtn is clicked
            allRestaurantsBtn.Clicked += (sender, e) =>
            {
				// Set the MasterDetails's Detail to RestaurantsTabbedPage
                App.MasterDetail.Detail = new NavigationPage(new RestaurantsTabbedPage())
                {
                    BarBackgroundColor = Color.Gray,
                };
                IsPresented = false;
            };

			// A method set when top10RestaurantsBtn is clicked
			top10RestaurantsBtn.Clicked += (sender, e) =>
            {
				// Set the MasterDetails's Detail to TopTenRes
				App.MasterDetail.Detail = new NavigationPage(new TopTenRes())
                {
                    BarBackgroundColor = Color.Gray,
                };
                IsPresented = false;
            };

			// A method set when recipeeBtn is clicked
			recipeBtn.Clicked += (sender, e) =>
            {
				// Set the MasterDetails's Detail to RecipePage
				App.MasterDetail.Detail = new NavigationPage(new RecipePage())
				{
					BarBackgroundColor = Color.Gray,
				};
				IsPresented = false;
            };

			// A method set when aboutBtn is clicked
			aboutBtn.Clicked += (sender, e) =>
			{
				// Set the MasterDetails's Detail to AboutPage
				App.MasterDetail.Detail = new NavigationPage(new AboutPage())
				{
					BarBackgroundColor = Color.Gray,
				};
				IsPresented = false;
			};

        }


	}
}

