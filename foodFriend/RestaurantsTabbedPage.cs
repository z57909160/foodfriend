﻿/*
 * Project Name: food Friends
 */

using System;

using Xamarin.Forms;

namespace foodFriend
{
	public class RestaurantsTabbedPage : TabbedPage
	{
		public RestaurantsTabbedPage()
		{
            // Set page's title
            Title = "Food Friend";

			// Create 2 new pages
            var mapPage = new foodFriendPage();
			var listPage = new RestaurantList();

			// Set BarBackgroundColor to Gray
			this.BarBackgroundColor = Color.Gray;

			// Set the 2 new pages' title
			mapPage.Title = "Map View";
			listPage.Title = "List View";

			// Set BarTextColor
			this.BarTextColor = Color.FromHex("#fcecdf");

			// Add the mapPage and listPage to page's children
			Children.Add(mapPage);
			Children.Add(listPage);

		}
	}
}