﻿/*
 * Project Name: food Friends
 */

using Xamarin.Forms;

namespace foodFriend
{
	public partial class App : Application
	{
		// Create MasterDetailPage
		public static MasterDetailPage MasterDetail;

		public App()
		{
			InitializeComponent();

			// Initialise Master Detail Page and its Detail
            MasterDetail = new navDrawer();
            MasterDetail.Detail = new NavigationPage(new RestaurantsTabbedPage()) {
                BarBackgroundColor = Color.Gray,
                Title = "Food Friend"
            };

			// Set MainPage to MasterDetail
            MainPage = MasterDetail;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

