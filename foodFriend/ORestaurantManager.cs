﻿/*
 * Project Name: food Friends
 */

// To add offline sync support: add the NuGet package Microsoft.Azure.Mobile.Client.SQLiteStore
   // to all projects in the solution and uncomment the symbol definition OFFLINE_SYNC_ENABLED
   // For Xamarin.iOS, also edit AppDelegate.cs and uncomment the call to SQLitePCL.CurrentPlatform.Init()
   // For more information, see: http://go.microsoft.com/fwlink/?LinkId=620342 
   //#define OFFLINE_SYNC_ENABLED
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

#if OFFLINE_SYNC_ENABLED
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
#endif

namespace foodFriend
{
    public partial class ORestaurantManager
    {
        static ORestaurantManager defaultInstance = new ORestaurantManager();
        MobileServiceClient client;

#if OFFLINE_SYNC_ENABLED
        IMobileServiceSyncTable<ORestaurant> resTable;
#else
        IMobileServiceTable<ORestaurant> resTable;
#endif

        private ORestaurantManager()
        {
            this.client = new MobileServiceClient(
                @"https://foodfriend.azurewebsites.net");

#if OFFLINE_SYNC_ENABLED
            var store = new MobileServiceSQLiteStore("localstore.db");
            store.DefineTable<ORestaurant>();

            //Initializes the SyncContext using the default IMobileServiceSyncHandler.
            this.client.SyncContext.InitializeAsync(store);

            this.resTable = client.GetSyncTable<ORestaurant>();
#else
            this.resTable = client.GetTable<ORestaurant>();
#endif
        }

        public static ORestaurantManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return resTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<ORestaurant>; }
        }

        public async Task<ObservableCollection<ORestaurant>> GetRestaurantAsync(bool syncItems = false)
        {
            try
            {
#if OFFLINE_SYNC_ENABLED
                if (syncItems)
                {
                    await this.SyncAsync();
                }
#endif
                IEnumerable<ORestaurant> items = await resTable
                    //.Where (res => res.Name =="res2")
                    .ToEnumerableAsync();

                return new ObservableCollection<ORestaurant>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }
        public async Task AddRestaurant(String name, String address, String phone, String img, int ranking, Restaurant.CategoryType category, string des, int hour_start, int hour_end, double longtitude, double latitude)
        {
            var res = new ORestaurant
            {
                Name = name,
                Address = address,
                Phone = phone,
                Img_Name = img,
                Ranking = ranking,
                Category = category.ToString(),
                Description = des,
                Hour_start = hour_start,
                Hour_end = hour_end,
                Longtitude = longtitude,
                Latitude = latitude,
                DBVersion = 1
            };
            await SaveTaskAsync(res);
        }
        public async Task InitialiseDB()
        {
            await AddRestaurant("Longrain Melbourne", "44 Little Bourke Street, CBD, Melbourne, VIC",
               "03 86578 528", "default.png", 1, Restaurant.CategoryType.Asian,
               "Longrain is a restaurant located in the CBD of Melbourne.The menu was inspired by Thai food.This restaurant fits perfectly for those who eats in group as servings are large.",
               18, 22, 144.971210, -37.810648);

            await AddRestaurant("Vanilla Bean", "287 Exhibition Street, CBD, Melbourne",
                "03 9663 0010", "default.png", 2, Restaurant.CategoryType.Australian,
                "Located in the CBD, this restaurant’s serves Australian cuisines.Our recommended dish is Avocado & Fritters here.",
                9, 21, 144.9689167, -37.8090647);

            await AddRestaurant("Gingerboy", "27 - 29 Crossley Street, CBD, Melbourne, VIC",
                "03 8592 7395", "default.png", 3, Restaurant.CategoryType.Asian,
                "Gingerboy is a unique restaurant that serves Tapas.Its well known for the atmosphere and its South East Asian food.", 12, 14, 144.9709028, -37.8112301);

            await AddRestaurant("Mitre Tavern", "5 Bank Place, CBD, Melbourne",
                "03 9602 5611", "default.png", 4, Restaurant.CategoryType.Western,
                "A restaurant located in CBD, Melbourne.Mitre Tavern is a place for fine dining, facilitated with live music, Wi - Fi, and full bar.Known best for its quality of steak.", 12, 22, 144.9603447, -37.8167782);

            await AddRestaurant("Dragon Boat", "203 Little Bourke Street, CBD, Melbourne",
                "03 8592 4948", "default.png", 5, Restaurant.CategoryType.Asian,
                "A Chinese restaurant located at the central business district of Melbourne.Open from morning until late at night, a perfect place to have Asian food.", 8, 23, 144.9666626, -37.8121126);

            await AddRestaurant("Sosta Cucina", "12 Errol Street, Melbourne, Victoria, Australia",
                "03 9329 2882", "default.png", 6, Restaurant.CategoryType.Western,
                "Sosta Cucina is an Italian restaurant located at North Melbourne.The restaurant is especially good for business meetings, special occasion dining, and romantic.Customer satisfaction of the restaurant is quite good, so it’s a recommended restaurant.", 12, 22, 144.9495684, -37.8048261);

            await AddRestaurant("Hot Poppy", "9 Errol St, Melbourne, Victoria 3051, Australia",
                "03 9326 9299", "default.png", 7, Restaurant.CategoryType.Australian,
                "A café with a good food, service, value, and atmosphere.Located in North of Melbourne, this café is good for eating breakfast or lunch.", 7, 17, 144.949047, -37.804762);

            await AddRestaurant("Albion Hotel", "171 Curzon St | North Melbourne, Melbourne, Victoria 3051, Australia",
                "03 9326 6575", "default.png", 8, Restaurant.CategoryType.Australian,
                "Place for those who wants to eat lunch or dinner.Located in North Melbourne, there’s also bar and pub here.", 18, 23, 144.9485744, -37.798879);

            await AddRestaurant("Akita", "Cnr Courtney & Blackwood Streets, Melbourne, Victoria 3051, Australia",
                "03 9326 5766 ", "default.png", 9, Restaurant.CategoryType.Asian,
                "Sushi Lovers ? It’s a place for you.For those who like Japanese food and Sushi, without a doubt, this is the right place for you to go.", 12, 22, 144.9540952, -37.8020156);

            await AddRestaurant("The Leveson", "46 Leveson St, Melbourne, Victoria 3051, Australia",
                "03 9328 1109", "default.png", 10, Restaurant.CategoryType.Australian,
                "Looking for a bar or pub ? You can go to The Leveson.A location that is nice with a friendly staff, making this restaurant feel like a 5 - star restaurant.", 12, 23, 144.9512688, -37.8037112);

            await AddRestaurant("Chez Dre", "Rear of 285 / 287 Coventry St | Access Via Alleyway, South Melbourne, Port Phillip, Victoria 3205, Australia",
                "03 9690 2688", "default.png", 11, Restaurant.CategoryType.Western,
                "Chez Drew serves French and Australian dishes, with gluten free options.", 7, 16, 144.963937, -37.833889);

            await AddRestaurant("Hercules Morse Kitchen + Bar", "283 Clarendon St, South Melbourne, Port Phillip, Victoria 3205, Australia",
                "0396909402", "default.png", 12, Restaurant.CategoryType.Australian,
                "Hercules Morse Kitchen is a restaurant that got a bar and serves Australian as well as International dishes.Also, the food has gluten free options.", 12, 23, 144.960959, -37.833236);

            await AddRestaurant("Peko Peko", "190 Wells St | Southbank, South Melbourne, Port Phillip, Victoria 3205, Australia",
                "96861109", "default.png", 13, Restaurant.CategoryType.Asian,
                "An Asian restaurant serving Chinese and Taiwanese food located in South Australia. People love the value and food being sold. Also, this restaurant is child friendly.",
                12, 21, 144.969504, -37.830649);
            await AddRestaurant("Simply Spanish", "116 Cecil St, South Melbourne, Port Phillip, Victoria 3205, Australia",
                "0396826100", "default.png", 14, Restaurant.CategoryType.Western,
                "Mediterranean, Spanish, and Latin food are all served here. Those who wants to eat the food can get it from morning until night, since the opening hours is from 8am to 10pm.",
                8, 22, 144.957196, -37.832473);
            await AddRestaurant("Big Huey's Diner", "315 Coventry St | Across the Road From South Melbourne Market, South Melbourne, Port Phillip, Victoria 3205, Australia",
                "03 9686 1122", "default.png", 15, Restaurant.CategoryType.Western,
                "Love burgers? Yes, Big Huey’s Diner makes a delicious burger and many more American foods you love. Located in South Melbourne, you can buy foods from here during breakfast, brunch, lunch, and dinner time.",
                12, 21, 144.963937, -37.833889);
            await AddRestaurant("Gertrude Street Enoteca", "229 Gertrude Street, Fitzroy, Melbourne",
                "03 9415 8262", "default.png", 16, Restaurant.CategoryType.Western,
                "Gertrude Street Enoteca is a restaurant that serves Western cuisine.  This restaurant is available at East Melbourne. The highlights of this restaurant includes gluten free options, outdoor seating, and vegetable friendly.",
                11, 23, 144.982188, -37.806129);

            await AddRestaurant("Geppetto Trattoria", "78A Wellington Parade, East Melbourne, Melbourne",
                "03 9417 4691", "default.png", 17, Restaurant.CategoryType.Western,
                "Geppetto Trattoria is a restaurant that is placed at East Melbourne that serves a delicious Italian cuisine. This restaurant also got a full bar.",
                12, 23, 144.987416, -37.816419);
            await AddRestaurant("My Sushi", "154 Wellington Parade, East Melbourne, Melbourne",
                "03 9419 0095", "default.png", 18, Restaurant.CategoryType.Asian,
                " Japanese restaurant that is placed at East Melbourne. Love Japanese Food? You can get it here.",
                11, 20, 144.984947, -37.816106);

            await AddRestaurant("Pho 365", "24 Smith Street, Collingwood, Melbourne",
                "03 9417 1881", "default.png", 19, Restaurant.CategoryType.Asian,
                "Pho is the main specialty of Pho 365. This restaurant is located at East Melbourne, and beside Pho, they also sell rice paper roll, rice vermicelli, and spring rolls.",
                10, 15, 144.982990, -37.807503);
            await AddRestaurant("Mina-no-ie", "33 Peel Street, Collingwood, Melbourne, VIC",
                "03 9417 7749", "default.png", 20, Restaurant.CategoryType.Asian,
                "A restaurant that suits for eating lunch or breakfast. Specialised on Japanese food, foods here have the Japanese taste that eater will like.",
                08, 16, 144.985603, -37.805408);

            await AddRestaurant("In Season Thai Cuisine", "279 Victoria Street, West Melbourne, Melbourne",
                "03 9328 2021", "default.png", 21, Restaurant.CategoryType.Asian,
                "A Thai restaurant located in West Melbourne that serves Thai entrée, soup, salad and grills.  Its highlights are delivery, takeaway, and eat at the restaurant.",
                17, 22, 144.951976, -37.805643);
            await AddRestaurant("Biryani House", "343 King Street, West Melbourne, Melbourne",
                "03 9329 4323", "default.png", 22, Restaurant.CategoryType.Asian,
                "Love Indian food? Then, Biryani House will be the restaurant that you will like. Serving dishes like rice dishes, lamb dishes, vegetarian dishes, chicken dishes, seafood dishes, snacks, and accompaniments, making this restaurant full of choices.",
                12, 22, 144.953557, -37.812244);
            await AddRestaurant("Le Taj", "74 Rosslyn Street, West Melbourne, Melbourne",
                "03 9329 8402", "default.png", 23, Restaurant.CategoryType.Asian,
                "Indian food is really popular, and Le Taj sells Indian food. Available at West Melbourne, this restaurant’s specialty are Dal Makhani, Kadhai Chicken, Beef Madras, Bhuna Lamb, and Paneer Kadhai Wala.",
                12, 22, 144.952021, -37.807744);
        }
        public async Task SaveTaskAsync(ORestaurant item)
        {
            if (item.Id == null)
            {
                await resTable.InsertAsync(item);
            }
            else
            {
                await resTable.UpdateAsync(item);
            }
        }

#if OFFLINE_SYNC_ENABLED
        public async Task SyncAsync()
        {
            ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

            try
            {
                await this.client.SyncContext.PushAsync();

                await this.resTable.PullAsync(
                    //The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
                    //Use a different query name for each unique query in your program
                    "allRestaurant",
                    this.resTable.CreateQuery());
            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult != null)
                {
                    syncErrors = exc.PushResult.Errors;
                }
            }

            // Simple error/conflict handling. A real application would handle the various errors like network conditions,
            // server conflicts and others via the IMobileServiceSyncHandler.
            if (syncErrors != null)
            {
                foreach (var error in syncErrors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        //Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
                }
            }
        }
#endif
    }
}
